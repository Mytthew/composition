package com.company;

public class Class extends Vehicle {
    private int doors;
    private int engineCapacity;

    public Class(String name, int doors, int engineCapacity) {
        super(name);
        this.doors = doors;
        this.engineCapacity = engineCapacity;
    }
}
